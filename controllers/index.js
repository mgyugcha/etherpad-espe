const Docker = require('dockerode')
const docker = new Docker({socketPath: '/var/run/docker.sock'})
const session = require('./session')
const database = require(__base + '/controllers/database')
const debug = require('debug')('app:controllers:index')

exports.index = function (req, res) {
  let data = { user: req.session.user, navPads: 'active'}
  getConfiguration(req.session.user.username)
    .then(config => {
      data.config = config
      res.render('index', data)
    })
    .catch(err => {
      console.error(err)
      res.status(500).send(err.message)
    })
}

exports.config = function (req, res) {
  let data = { user: req.session.user, navConfig: 'active' }
  getConfiguration(req.session.user.username)
    .then(config => {
      data.config = config
      res.render('config', data)
    })
    .catch(err => {
      console.error(err)
      res.status(500).send(err.message)
    })
}

exports.login = {
  get: function (req, res) {
    res.render('login')
  },
  post: function (req, res) {
    session.userLogin(req.body, req.session)
      .then(response => {
        res.status(response ? 200 : 400).end()
      })
      .catch(err => {
        console.error('Login post.', err)
        res.status(500).send(err.message)
      })
  }
}

exports.logout = function (req, res) {
  req.session.destroy(err => {
    if (err) {
      res.send(err)
    } else {
      res.redirect('/login')
    }
  })
}

function getConfiguration(username) {
  return new Promise((resolve, reject) => {
    let config = {
      host: '10.9.7.135'
    }
    let collection = database.getCollection(database.collections.USERS)
    console.log(username)
    let promises = [
      collection.findOne({'username': username}),
      docker.listContainers({filters: {name: [username]}}),
    ]

    Promise.all(promises)
      .then(result => {
	config.port = result[0].etherpad.port
	config.running = result[1].length === 1
	resolve(config)
      })
      .catch(err => {
	reject(err)
      })
  })
  
}
