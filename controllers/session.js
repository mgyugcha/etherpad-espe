const database = require(__base + '/controllers/database')
const usercollection = database.collections.USERS
const debug = require('debug')('etherpad-espe:controllers:session')

exports.session = function (req, res, next) {
  let url = req.originalUrl
  if (req.session.user) {
    if (url.includes('/login') || url.includes('/signup')) {
      res.redirect('/')
    } else {
      next()
    }
  } else {
    debug('no se ha iniciado sesión')
    if (url.includes('/login') || url.includes('/signup')) {
      debug('la url  incluye')
      next()
    } else {
      res.redirect('/login')
    }
      
  }
}

exports.userLogin = function (user, session) {
  return new Promise((resolve, reject) => {
    let collection = database.getCollection(usercollection)
    collection.findOne(user)
      .then(data => {
        if (data) {
          session.user = getSessionUserData(data)
          resolve(true)
        } else {
          resolve(false)
        }
      })
      .catch(err => { reject(err) })
  })
}

function getSessionUserData(data) {
  return {
    '_id': data._id,
    'username': data.username,
    'name': data.name,
    'etherpad': data.etherpad
  }
}
