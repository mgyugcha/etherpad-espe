const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://127.0.0.1/etherpad-espe'

var state = {
  database: null,
}

exports.collections = {
  USERS: 'users',
  PADS: 'pads',
  CONFIGURATIONS: 'configurations',
}

exports.connect = function () {
  return new Promise((resolve, reject) => {
    if (state.database) return resolve()
    MongoClient.connect(url)
      .then(database => {
        state.database = database
        resolve()
      })
      .catch(err => { reject(err) })
  })
}

exports.get = function() {
  return state.database
}

exports.getCollection = function (name) {
  return state.database.collection(name)
}

// exports.close = function () {
//   if (state.db) {
//     state.db.close(function(err, result) {
//       state.db = null
//       state.mode = null
//       done(err)
//     })
//   }
// }
