const restuser = '/rest/users'

angular.module('config', [])
  .controller('main', function ($scope, $http) {
    $scope.username = null

    $scope.v = {
      password: {
	class: '',
	change: function () {
          let pass = $scope.duser.password
          let passRepeat = $scope.duser.passwordRepeat
          this.class = !(pass && passRepeat) ? '' :
            (pass === passRepeat) ? 'has-success' : 'has-danger'
	}
      },
      user: {
	class: '',
	change: function () {
          let username = $scope.user.username
	  if (username === $scope.username || !username) {
	    this.class = ''
	    return
	  }
          let self = this
          $http.head(`${restuser}/${username}`)
            .then(res => {
	      this.class = res.status === 200 ? 'has-danger' : 'has-success'
            }, res => {
              console.error(res.data)
            })
	}
      }
    }

    $scope.init = function () {
      $http.get(`${restuser}/me`)
    	.then(res => {
	  $scope.username = res.data.username
    	  $scope.user = res.data
    	}, res => {
    	  console.error(res.data)
    	  notify.failed()
    	})
    }

    $scope.submitInfo = function () {
      if ($scope.v.user.class === 'has-danger') {
	notify.formWarning()
	return
      }
      $http.put(`${restuser}/me`, $scope.user)
	.then(res => {
	  notify.saved()
	}, res => {
	  console.error(res.data)
	  notify.failed()
	})
    }

    $scope.submitPassword = function () {
      if ($scope.v.password.class === 'has-danger') {
	notify.formWarning()
	return
      }
      $http.put(`${restuser}/me/password`, $scope.duser)
	.then(res => {
	  notify.saved()
	}, res => {
	  console.error(res.data)
	  if (res.status === 400)
	    notify.warning('Contraseña incorrecta', 'Compruebe la contraseña')
	  else
	    notify.failed()
	})
    }

    $scope.statusServidor = function (status) {
      console.log(status)
      $scope.status = {
	type: status,
	disabledBtn: false,
	class: status ? 'btn-danger' : 'btn-success',
	textBtn: status ? 'Detener servidor' : 'Correr servidor',
	text: status ? 'El servidor se encuentra activo' :
	  'Cuando el servidor esta inactivo no se puede acceder a los pads'
      }
    }

    $scope.startServer = function () {
      $scope.status.disabledBtn = true
      $scope.status.textBtn = 'Cambiando estado ...'
      $http.put(`${restuser}/me/toggle`, { status: !$scope.status.type })
	.then(res => {
	  notify.saved(() => {
	    location.reload()
	  })
	}, res => {
	  notify.failed()
	})
    }

  })
