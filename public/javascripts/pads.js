const mainrest = '/rest/pads'

String.prototype.replaceAll = function(search, replacement) {
  var target = this
  return target.replace(new RegExp(search, 'g'), replacement)
}

function mainController($scope, $http, $filter) {

  $scope.pads = []

  $scope.v = {
    idpad: {
      change: function () {
	let string = $filter('lowercase')($scope.pad.title)
	if (!string) return
	string = string.replace(new RegExp(' ', 'g'), '-')
	string = encodeURI(string)
	$scope.pad.url = string
      }
    }
  }

  $scope.init = function () {
    $http.get(mainrest)
      .then(res => {
	$scope.pads = res.data
      })
  }

  $scope.delete = function (id, title) {
    notify.swal({
      title: '¿Borrar?',
      text: `Se eliminará el pad ${title}`,
      showCancelButton: true,
      confirmButtonClass: 'btn-danger',
      confirmButtonText: 'Borrar',
      closeOnConfirm: false
    }, () => {
      $http.delete(`${mainrest}/${id}`)
	.then(res => {
	  $scope.init()
	  notify.deleted()
	}, res => {
	  console.error(res.data)
	  notify.serverError()
	})
    })
  }

  $scope.submit = function () {
    console.log($scope.pad)
    $http.post(mainrest, $scope.pad)
      .then(res => {
	notify.saved()
	$scope.init()
	$('#padModal').modal('hide')
      }, res => {
	console.error(res.data)
	notify.serverError()
      })
  }
}

angular.module('pads', [])
  .controller('main', mainController)
