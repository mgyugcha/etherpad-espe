const router = require('express').Router()
const Docker = require('dockerode')
const docker = new Docker({socketPath: '/var/run/docker.sock'})
const get = require(__base + '/controllers/utils').getProperty
const database = require(__base + '/controllers/database')
const ObjectID = require('mongodb').ObjectID
const maincollection = database.collections.USERS
const confQuery = { '_id': 'config' }

router.route('/')
  .post((req, res) => {
    let user = req.body
    let query = { username: user.username }
    let nextPort = null
    let promises = []
    let configColName = database.collections.CONFIGURATIONS
    let configCollection = database.getCollection(configColName)
    let collection = database.getCollection(maincollection)

    promises.push(collection.findOne(query))
    promises.push(configCollection.findOne(confQuery))

    Promise.all(promises)
      .then(docs => {
        nextPort = docs[1] ?
	  ( docs[1].currentPort ?
	    docs[1].currentPort + 1 : 9001) : 9001

        if (docs[0]) {
          // error
        } else {
          let schema = {
            username: get(user, 'username'),
            password: get(user, 'password'),
            name: get(user, 'name'),
            email: get(user, 'email'),
            // etherpad
            etherpad: {
              name: 'Sin título',
              port: nextPort,
              creation: new Date(),
            }
          }
          return collection.insertOne(schema)
        }
      })
      .then(() => {
        let dockerConfiguration = {
          Image: 'tvelocity/etherpad-lite',
          Env: [
            'ETHERPAD_DB_HOST=etherpad-mariadb',
            'ETHERPAD_DB_PASSWORD=etherpad2017',
            'ETHERPAD_DB_NAME=' + user.username,
          ],
          ExposedPorts: { '9001/tcp': {} },
          HostConfig: {
            NetworkMode: 'etherpad-network',
            PortBindings: {
              '9001/tcp': [{ HostIp: '', HostPort: nextPort.toString() }]
            }
          },
          name: user.username
        }

        return docker.createContainer(dockerConfiguration)
      })
      .then(container => {
	return container.start()
      })
      .then(data => {
	let update = { currentPort: nextPort }
	let options = { upsert: true }
	return configCollection.updateOne(confQuery, update, options)
      })
      .then(() => {
        res.end()
      })
      .catch(err => {
        console.error(err.message)
        res.status(500).send(err)
      })
  })

router.route('/me')
  .get((req, res) => {
    let query = { '_id': new ObjectID(req.session.user._id) }
    let project = { 'password': 0 }
    let collection = database.getCollection(maincollection)
    collection.findOne(query, project)
      .then(doc => {
        res.json(doc)
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })
  .put((req, res) => {
    let filter = { '_id': new ObjectID(req.session.user._id) }
    delete req.body._id
    let update = { $set: req.body }
    let collection = database.getCollection(maincollection)
    collection.update(filter, update)
      .then(() => {
        req.session.user['name'] = req.body.name
        req.session.user['etherpad'].name = req.body.etherpad.name
        res.end()
      })
      .catch(err => {
        console.error(err)
        res.status(500).send(err.message)
      })
  })

router.put('/me/toggle', (req, res) => {
  let status = req.body.status
  var container = docker.getContainer(req.session.user.username)
  let promise = status ? container.start() : container.stop()

  promise
    .then(() => {
      res.end()
    })
    .catch(err => {
      console.error(err)
      res.status(500).send(err.message)
    })
})

router.put('/me/password', (req, res) => {
  let query = {
    '_id': new ObjectID(req.session.user._id),
    'password': req.body.oldPassword
  }
  let collection = database.getCollection(maincollection)
  collection.count(query)
    .then(length => {
      if (length === 0) {
        return null
      } else {
        let update = { $set: { 'password': req.body.password } }
        return collection.update(query, update)
      }
    })
    .then(result => {
      res.status(result === null ? 400 : 200).end()
    })
    .catch(err => {
      console.error(err)
      res.status(500).end()
    })
})

router.route('/:id')
  .head((req, res) => {
    let query = { username: req.params.id }
    let collection = database.getCollection(maincollection)
    collection.count(query)
      .then(length => { res.status(length === 0 ? 204 : 200).end() })
      .catch(err => {
        console.error(err.message)
        res.status(500).send(err.message)
      })
  })

// exports.post = function (req, res) {
//   Users.insert(req.body)
//     .then(() => {
//       res.end()
//     })
//     .catch(err => {
//       console.error(err)
//       res.status(err.httpCode || 500).send(err.message)
//     })
// }

// exports.get = function (req, res) {
//   let query = {
//     '_id': req.params.id
//   }

//   Users.get(query)
//     .then(doc => {
//       res.json(doc)
//     })
//     .catch(err => {
//       console.error(err)
//       res.send(err.message)
//     })
// }

module.exports = router
