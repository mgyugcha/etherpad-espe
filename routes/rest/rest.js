const router = require('express').Router()
const Users = require('./users')

// usuarios

router.use('/users', require('./users'))
router.use('/pads', require('./pads'))

// router.route('/users')
//   .post(Users.post)

// router.route('/users/:id')
//   .get(Users.head)
//   .get(Users.get)

module.exports = router
